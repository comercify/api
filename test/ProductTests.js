process.env.NODE_ENV = 'test';

//Require the dev-dependencies
const chai       = require('chai'),
  chaiHttp       = require('chai-http'),
  server         = require('../index'),
  mongoose       = require('mongoose'),
  Product        = mongoose.model('Product'),
  User           = mongoose.model('User'),
  {readFileSync} = require("fs"),
  jwt            = require('jsonwebtoken')


chai.use(chaiHttp);

describe('Products', () => {

  const product = new Product({
    "name":"teste",
    "product":"test",
    "description": "a description test",
    "quantity": 4,
    "paid_value":1,
    "sell_value":1,
    "max_discount":0.5
  })

  const productJSON = {
    "name":"teste",
    "product":"test",
    "description": "a description test",
    "quantity": 4,
    "paid_value":1,
    "sell_value":1,
    "max_discount":0.5
  }

  const email = 'victor.duarte.oliveira@gmail.com'
  User.findOne({email: email},(err,user)=> {
    const token = 'Bearer ' + jwt.sign(user, 'shhhhhhared-secret', {
        expiresIn: 60 * 60 * 24 // expires in 24 hours
      });

    describe('/POST methods', () => {

      it('it should create a product without image', (done) => {

        chai.request(server)
          .post('/product/create-product')
          .set('Authorization', token)
          .send(product)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            product.remove({ _id: product._id }, (err) => {})
            done();
          });


      })

      it('it should create a product with image', (done) => {
        chai.request(server)
          .post('/product/create-product')
          .set('Authorization', token)
          .attach('image', readFileSync(__dirname+'/test.png'), __dirname+'/test.png')
          .field('body',JSON.stringify(productJSON))
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            const id = res.body.data._id
            Product.remove({ _id: id }, (err) => {})
            done()
          });

      })


      it('it should get a product given the name part', (done) => {

        let name = {"name": "teste"}

        chai.request(server)
          .post('/product/search-product-by-name')
          .send(name)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            done();
          });


      })

    })


    describe('/GET products', () => {

      it('it should GET all the products', (done) => {
        chai.request(server)
          .get('/product/get-all-products')
          .end((err, res) => {
            res.should.have.status(200)
            done()
          });
      });

      it('it should GET product by id', (done) => {

        product.save((err,product)=>{
          chai.request(server)
            .get('/product/get-product-by-id/'+product._id)
            .end((err, res) => {
              res.should.have.status(200)
              res.body.should.be.a('object')
              res.body.should.have.property('success')
              res.body.data.should.have.property('name')
              res.body.data.should.have.property('product')
              res.body.data.should.have.property('paid_value')
              product.remove({ _id: product._id }, (err) => {})
              done()
            });
        })


      });

    });


    describe('/PUT/:id product', () => {
      it('it should UPDATE a product given the id', (done) => {

        product.save((err, product) => {
          chai.request(server)
            .put('/product/update-product-by-id/' + product._id)
            .set('Authorization', token)
            .send({"name": "teste update", "email": "testeUpdate@teste.com"})
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.data.should.have.property('name').eql('teste update');
              res.body.data.should.have.property('email').eql('testeUpdate@teste.com');
              product.remove({ _id: product._id }, (err) => {})
              done();
            });

        });

      })

    })

    describe('/DELETE/:id product', () => {

      it('it should DELETE a product given the id', (done) => {

        product.save((err, product) => {
          chai.request(server)
            .delete('/product/delete-product-by-id/' + product._id)
            .set('Authorization', token)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('success').eql(true);
              product.remove({ _id: product._id }, (err) => {})
              done();
            });
        });
      });
    })
  })


});
