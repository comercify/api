process.env.NODE_ENV = 'test';

//Require the dev-dependencies
const chai     = require('chai'),
  chaiHttp = require('chai-http'),
  server   = require('../index'),
  mongoose = require('mongoose'),
  User 	   = mongoose.model('User')

chai.use(chaiHttp);

describe('Auth', () => {

  const auth = {
    "email" : "victor.duarte.oliveira@gmail.com",
    "password" : "123321"
  }

  describe('/POST methods', () => {

    it('it should authenticate a user', (done) => {
      chai.request(server)
        .post('/authenticate')
        .send(auth)
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('data');
          res.body.should.have.property('success').eql(true);
          done();
        });
    })

  })

});
