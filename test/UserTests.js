process.env.NODE_ENV = 'test';

//Require the dev-dependencies
const chai     = require('chai'),
  chaiHttp = require('chai-http'),
  server   = require('../index'),
  mongoose = require('mongoose'),
  User 	   = mongoose.model('User'),
  jwt      = require('jsonwebtoken')

chai.use(chaiHttp);

describe('Users', () => {

  const user = new User({"name":"teste","email":"teste@teste.com","password": "123123", "gender":"mano","user_level":1})

  const email = 'victor.duarte.oliveira@gmail.com'
  User.findOne({email: email},(err,userAuth)=> {
    const token = 'Bearer ' + jwt.sign(userAuth, 'shhhhhhared-secret', {
        expiresIn: 60 * 60 * 24 // expires in 24 hours
      });
    describe('/POST methods', () => {

      it('it should create a user', (done) => {

        chai.request(server)
          .post('/user/create-user')
          .set('Authorization', token)
          .send(user)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            user.remove({ _id: user._id }, (err) => {})
            done();
          });


      })

      it('it should get a user given the name part', (done) => {

        let name = {"name": "teste"}

        chai.request(server)
          .post('/user/search-user-by-name')
          .set('Authorization', token)
          .send(name)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            done();
          });


      })

    })


    describe('/GET users', () => {

      it('it should GET all the users', (done) => {
        chai.request(server)
          .get('/user/get-all-users')
          .set('Authorization', token)
          .end((err, res) => {
            res.should.have.status(200)
            done()
          });
      });

      it('it should GET user by id', (done) => {

        user.save((err,user)=>{
          chai.request(server)
            .get('/user/get-user-by-id/'+user._id)
            .set('Authorization', token)
            .end((err, res) => {
              res.should.have.status(200)
              res.body.should.be.a('object')
              res.body.should.have.property('success')
              res.body.data.should.have.property('name')
              res.body.data.should.have.property('email')
              res.body.data.should.have.property('gender')
              res.body.data.should.have.property('user_level')
              user.remove({ _id: user._id }, (err) => {})
              done()
            });
        })


      });

    });


    describe('/PUT/:id user', () => {
      it('it should UPDATE a user given the id', (done) => {

        user.save((err, user) => {
          chai.request(server)
            .put('/user/update-user-by-id/' + user._id)
            .set('Authorization', token)
            .send({"name": "teste update", "email": "testeUpdate@teste.com"})
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.data.should.have.property('name').eql('teste update');
              res.body.data.should.have.property('email').eql('testeUpdate@teste.com');
              user.remove({ _id: user._id }, (err) => {})
              done();
            });

        });

      })

    })

    describe('/DELETE/:id user', () => {

      it('it should DELETE a user given the id', (done) => {

        user.save((err, user) => {
          chai.request(server)
            .delete('/user/delete-user-by-id/' + user._id)
            .set('Authorization', token)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('success').eql(true);
              user.remove({ _id: user._id }, (err) => {})
              done();
            });
        });
      });
    })
  })

});
