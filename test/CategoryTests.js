process.env.NODE_ENV = 'test';

//Require the dev-dependencies
const chai     = require('chai'),
  chaiHttp = require('chai-http'),
  server   = require('../index'),
  mongoose = require('mongoose'),
  Category 	   = mongoose.model('Category'),
  User 	   = mongoose.model('User'),
  jwt            = require('jsonwebtoken')

chai.use(chaiHttp);

describe('Categories', () => {

  const category = new Category({"name":"teste"})
  const email = 'victor.duarte.oliveira@gmail.com'
  User.findOne({email: email},(err,user)=>{
    const token = 'Bearer ' + jwt.sign(user, 'shhhhhhared-secret', {
        expiresIn : 60*60*24 // expires in 24 hours
      });

    describe('/POST methods', () => {

      it('it should create a category', (done) => {

        chai.request(server)
          .post('/category/create-category')
          .set('Authorization', token)
          .send(category)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            category.remove({ _id: category._id }, (err) => {})
            done();
          });


      })

      it('it should get a category given the name part', (done) => {

        let name = {"name": "teste"}

        chai.request(server)
          .post('/category/search-category-by-name')
          .send(name)
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('success').eql(true);
            done();
          });


      })

    })


    describe('/GET categories', () => {

      it('it should GET all the categories', (done) => {
        chai.request(server)
          .get('/category/get-all-categories')
          .end((err, res) => {
            res.should.have.status(200)
            done()
          });
      });

      it('it should GET category by id', (done) => {

        category.save((err,category)=>{
          chai.request(server)
            .get('/category/get-category-by-id/'+category._id)
            .end((err, res) => {
              res.should.have.status(200)
              res.body.should.be.a('object')
              res.body.should.have.property('success')
              res.body.data.should.have.property('name')
              category.remove({ _id: category._id }, (err) => {})
              done()
            });
        })


      });

    });


    describe('/PUT/:id category', () => {
      it('it should UPDATE a category given the id', (done) => {

        category.save((err, category) => {
          chai.request(server)
            .put('/category/update-category-by-id/' + category._id)
            .set('Authorization', token)
            .send({"name": "teste update", "email": "testeUpdate@teste.com"})
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.data.should.have.property('name').eql('teste update');
              res.body.data.should.have.property('email').eql('testeUpdate@teste.com');
              category.remove({ _id: category._id }, (err) => {})
              done();
            });

        });

      })

    })

    describe('/DELETE/:id category', () => {

      it('it should DELETE a category given the id', (done) => {

        category.save((err, category) => {
          chai.request(server)
            .delete('/category/delete-category-by-id/' + category._id)
            .set('Authorization', token)
            .end((err, res) => {
              res.should.have.status(200);
              res.body.should.be.a('object');
              res.body.should.have.property('success').eql(true);
              category.remove({ _id: category._id }, (err) => {})
              done();
            });
        });
      });
    })
  })
});
