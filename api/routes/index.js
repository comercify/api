
'use strict'

/**
 * Module Dependencies
 */
const _  = require('lodash'),
  errors = require('restify-errors'),
  Multer = require('multer'),
  jwt    = require('restify-jwt');


const multer = Multer({
  storage: Multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024 // no larger than 5mb, you can change as needed.
  }
});

/**
 * Model Schema
 */
const User    = require('../models/User'),
      Product = require('../models/Product'),
      Category = require('../models/Category'),
      Checkout = require('../models/Checkout'),
      Address = require('../models/Address'),
      UserProduct = require('../models/UserProduct'),
      Transaction = require('../models/Transaction')

/**
 * Controllers
 */

const checkoutController    = require('../controllers/CheckoutController'),
      productController     = require('../controllers/ProductController'),
      imageController       = require('../controllers/ImageController'),
      authController        = require('../controllers/AuthController'),
      categoryController    = require('../controllers/CategoryController'),
      correioController     = require('../controllers/CorreioController'),
      userController        = require('../controllers/UserController'),
      transactionController = require('../controllers/TransactionController'),
      filterController      = require('../controllers/FilterController')


/**
 * MiddleWares
 */

const verifyAdmin = require('../middleware/VerifyAdmin')


/**
 * Routes
 */

/**
 * auth
 */

server.post('/authenticate', authController.authenticate)

/**
 * Filter
 */
server.get('/filter/boundaries', filterController.getFilterBoundaries)
/**
 * products
 */

/*
 * non auth
 */

server.get('/product/get-all-products',                 productController.getAllProducts)
server.post('/product/search-product-by-name',          productController.searchProductByName)
server.get('/product/get-product-by-id/:product_id',    productController.getProductById)

/*
 * authed
 */

server.post('/product/create-product',                  jwt({secret: 'shhhhhhared-secret'}), verifyAdmin,
  productController.createProduct)
server.put('/product/update-product-by-id/:product_id', jwt({secret: 'shhhhhhared-secret'}), verifyAdmin,
  productController.updateProductById)
server.del('/product/delete-product-by-id/:product_id', jwt({secret: 'shhhhhhared-secret'}), verifyAdmin,
  productController.deleteProductById)

/**
 * images
 */

server.post('/image/upload-image', imageController.uploadImage)

/**
 * Correios
 */
server.post('/correio/calc-preco-prazo', correioController.calcPrecoPrazo)
server.post('/correio/calc-preco',       correioController.calcPreco)

/**
 * categories
 */

server.get('/category/get-all-categories',                 categoryController.getAllCategories)
server.post('/category/search-category-by-name',           categoryController.searchCategoryByName)
server.get('/category/get-category-by-id/:category_id',    categoryController.getCategoryById)

/**
 * users
 */

/*
 * authed
 */
server.post('/user/create-user', userController.createUser)
server.put('/user/update-user-by-id/:user_id',  userController.updateUserById)
server.get('/user/get-user-by-id/:user_id',    jwt({secret: 'shhhhhhared-secret'}),
  userController.getUserById)

/*
 * non authed
 */
server.post('/user/create-user', userController.createUser)
server.get('/user/get-checkouts-by-user-id/:user_id', userController.getCheckoutsByUserId)

/*
 * admin
 */
server.post('/user/search-user-by-name',       jwt({secret: 'shhhhhhared-secret'}), verifyAdmin,
  userController.searchUserByName)
server.get('/user/get-all-users',
  userController.getAllUsers)


/**
 * checkout
 */

server.post('/checkout', checkoutController.checkout)
server.get('/checkout/get-checkout-session-id', checkoutController.startCheckoutSession)
server.post('/checkout/create-checkout', checkoutController.createCheckout)
server.get('/checkout/get-all-checkouts', checkoutController.getAllCheckouts)
server.get('/checkout/get-checkout-by-id/:checkout_id', checkoutController.getCheckoutById)
server.post('/checkout/callback-transaction', checkoutController.callbackTransaction)

//temp
// server.get('/checkout/delete-all-checkouts', checkoutController.deleteAllCheckouts)


/**
 * transaction
 */

server.get('/transaction//get-all-transactions', transactionController.getAllTransactions)

/**
 * categories
 */

server.post('/category/create-category',                   jwt({secret: 'shhhhhhared-secret'}), verifyAdmin,
  categoryController.createCategory)
server.put('/category/update-category-by-id/:category_id', jwt({secret: 'shhhhhhared-secret'}), verifyAdmin,
  categoryController.updateCategoryById)
server.del('/category/delete-category-by-id/:category_id', jwt({secret: 'shhhhhhared-secret'}), verifyAdmin,
  categoryController.deleteCategoryById)


// server.get('/user/get-all-users',              jwt({secret: 'shhhhhhared-secret'}), verifyAdmin,
//   userController.getAllUsers)

server.del('/user/delete-user-by-id/:user_id', jwt({secret: 'shhhhhhared-secret'}), verifyAdmin, userController.deleteUserById)

