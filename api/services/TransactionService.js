const mongoose = require('mongoose'),
  service     = require('./Service'),
  Transaction     = mongoose.model('Transaction')

let checkoutService = require('./CheckoutService')

const TransactionService = {

  createTransaction(transaction){
    checkoutService = require('./CheckoutService')
    const transactionModel = new Transaction(transaction)
    checkoutService.addTransaction(transactionModel._checkout,transactionModel._id)
    return service.create(transactionModel);
  },
  updateTransactionById(id,transaction){
    return service.updateById(Transaction,id,transaction)
  },
  getTransactionById(id){
    return service.getById(Transaction,id)
  },
  getTransactionByCode(code){
    return new Promise((fulfill,reject)=>{
      Transaction.findOne({ code: code }, function(err, doc) {
        if (err)
          reject(err)
        fulfill(doc)
      });
    })

  }

}

module.exports = TransactionService

