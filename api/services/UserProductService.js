const mongoose = require('mongoose'),
  service     = require('./Service'),
  UserProduct     = mongoose.model('User-Product')

let checkoutService = require('./CheckoutService')

const UserProductService = {

  createUserProduct(userProduct){
    checkoutService = require('./CheckoutService')
    const userProductModel = new UserProduct(userProduct)
    // checkoutService.addUserProduct(userProductModel._checkout,userProductModel._id)
    return service.create(userProductModel);
  },
  createProducts(userProducts){
    const newArr = userProducts.map(userProduct=>{
      return this.createUserProduct(userProduct)
    })
    return Promise.all(newArr)
  },
  updateUserProductById(id,userProduct){
    return service.updateById(UserProduct,id,userProduct)
  },
  getUserProductById(id){
    return service.getById(UserProduct,id)
  },
  getUserProductByCode(code){
    return new Promise((fulfill,reject)=>{
      UserProduct.findOne({ code: code }, function(err, doc) {
        if (err)
          reject(err)
        fulfill(doc)
      });
    })

  }

}

module.exports = UserProductService

