const mongoose = require('mongoose'),
  service     = require('./Service'),
  Product     = mongoose.model('Product')

const ProductService = {

  createProduct(product){
    return service.create(product);
  },
  updateProductById(id,product){
    return service.updateById(Product,id,product)
  },
  getProductById(id){
    return service.getById(Product,id)
  },
  checkProducts(products){
    let promisses =  products.map(product=>{
      return this.checkProduct(product)
    })

    return Promise.all(promisses)
  },
  checkProduct(product){
    return this.getProductById(product._id).then(productInStock=>{

      product.id          = product._id;
      product.description = productInStock.description;
      product.weight      = productInStock.weight || 1;

      if(product.qtd > productInStock.quantity){
        product.qtd = 0;
      }
      return product;
    })
  }

}

module.exports = ProductService

