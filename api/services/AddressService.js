const mongoose = require('mongoose'),
      Address     = mongoose.model('Address'),
      service  = require('./Service')

const AddressService = {
  getAddressById(id){
    return service.getById(Address,id)
  },
  updateAddressById(id,address){
    return service.updateById(Address,id,address)
  },
  createAddress(address){
    return service.create(address);
  },
}

module.exports = AddressService

