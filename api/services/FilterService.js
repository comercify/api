const mongoose = require('mongoose'),
  User     = mongoose.model('User'),
  Category     = mongoose.model('Category'),
  Product     = mongoose.model('Product'),
  service  = require('./Service')

const FilterService = {
  getBoundaries(){
    const gendersP    = this.getGenders()
    const categoriesP = this.getCategories()
    const maxPriceP   = this.getPrice(-1) //-1 for max
    const minPriceP   = this.getPrice(1) // 1 for min

    return Promise.all([gendersP,categoriesP,maxPriceP,minPriceP])

  },
  getGenders(){
    return new Promise((fulfill,reject)=> {
      User.distinct('gender',(err, genders)=>{
        if(err)
          reject(err)
        fulfill(genders)
      })
    })
  },

  getCategories(){
    return new Promise((fulfill,reject)=> {
      Category.distinct('name',(err, genders)=>{
        if(err)
          reject(err)
        fulfill(genders)
      })
    })
  },

  getPrice(order){
    return new Promise((fulfill,reject)=> {
      Product.find().sort({sell_value:order}).limit(1).exec((err,data)=>{
        if(err)
          reject(err)
        fulfill(data)
      })

    })
  },


}

module.exports = FilterService

