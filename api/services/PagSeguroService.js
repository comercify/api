const mongoose = require('mongoose'),
  service     = require('./Service'),
  transactionService     = require('./TransactionService'),
  pagseguroConfig = require('../../config/PagSeguro.json'),
  request = require('request'),
  xml2json = require('xml2json'),
  json2xml = require('jstoxml'),
  http = require(`https`),
  { StringDecoder } = require('string_decoder'),
  decoder = new StringDecoder('utf8');

const TOKEN = pagseguroConfig.token_sandbox
const HOST  = pagseguroConfig.host_sandbox
const EMAIL  = pagseguroConfig.email
const TRANSACTION_INFO = pagseguroConfig.transactionInfo
const PAGSEGURO_URL_START_SESSION = `https://${HOST}/v2/sessions`
const PAGSEGURO_URL_CHECKOUT = `https://${HOST}/v2/transactions`

let options = {
  method: "POST",
  headers: {
    "Content-type": "application/xml"
  },
  host : HOST,
  form: {
    email : EMAIL,
    token: TOKEN
  }
};

const PagSeguroService = {

  getUrl(path){
    return path + "?email="+ EMAIL + "&token="+TOKEN;
  },

  startSession(){
    options.url = PAGSEGURO_URL_START_SESSION

    return new Promise((fulfill,reject)=>{
      request.post(options,function(err, resp, body) {
        if(err)
          reject(err)

        const json = JSON.parse(xml2json.toJson(body));

        fulfill(json.session)
      })
    })
  },
  mountPaymentRequest(user, address, shipp, payment, products){

    const paymentObj = {
      mode: TRANSACTION_INFO.mode,
      currency: TRANSACTION_INFO.currency,
      receiverEmail : TRANSACTION_INFO.receiverEmail,
      notificationURL: TRANSACTION_INFO.notification_url,
      sender: {},
      items: {},
      // reference: null,
      // shipping: {},
      // extraAmount: null,
      method: payment.method,
      creditCard: {},

      // bank: {
      //   name: null,
      // },
      // dynamicPaymentMethodMessage: {
      //   creditCard: null,
      //   boleto: null
      // },
      // promoCode: null
    };

    const sender = {
      hash: user.sender_hash,
      name: user.name +" "+user.last_name,
      email: user.email,
      phone: {
        areaCode: user.area_code,
        number: user.phone_number,
      },
      documents: {
        document: {
          type: "CPF",
          value: user.cpf
        }
      },
      bornDate: user.birth
    }

    const shipping = {
      address: {
        street: address.logradouro,
        number: address.numero,
        complement: address.complemento,
        district: address.bairro,
        city: address.localidade,
        state: address.uf,
        country: "BRA",
        postalCode: address.cep
      },
      type: shipp.type,
      cost: shipp.cost
      // addressRequired: null,
    }

    const creditCard = {
      token: payment.token,
      capture: true,
      installment: {
        quantity: payment.installment.quantity,
        noInterestInstallmentQuantity: payment.installment.noInterestInstallmentQuantity,
        value: payment.installment.value ? (payment.installment.value).toFixed(2) : ''
      },
      holder: {
        name: user.name +" "+user.last_name,
        documents: {
          document: {
            type: "CPF",
            value: user.cpf
          }
        },
        birthDate: user.birth,
        phone: {
          areaCode: user.area_code,
          number: user.phone_number,
        },
      },
      billingAddress: {}
    }

    const items = [];

    for(let product of products) {
      items.push({item:{
        id: product.id,
        description: product.description,
        amount: parseFloat(product.sell_value).toFixed(2),
        quantity: product.qtd,
      }})
    }

    return new Promise((fulfill, reject)=>{

      paymentObj.items = items
      paymentObj.sender = sender
      paymentObj.shipping = shipping
      paymentObj.creditCard = creditCard
      paymentObj.creditCard.billingAddress = shipping.address

      const xml = json2xml.toXML({payment: paymentObj})
      fulfill('<?xml version="1.0"?>'+xml)
      reject(paymentObj)
    })
  },

  pay(xml){

    options.uri = PAGSEGURO_URL_CHECKOUT
    options.headers['Content-Length'] = Buffer.byteLength(xml)
    options.path = this.getUrl("/v2/transactions")

    return new Promise((fulfill,reject)=>{

      let req = http.request( options, function( res ) {

        let buffer = "";
        res.on( "data", data => {
          buffer = buffer + data;

        } );
        res.on( "end", ()=> {
          const jsonObj = JSON.parse(xml2json.toJson(decoder.write(buffer)));
          fulfill( jsonObj );
        });

      });

      req.on('error', err=>{
        reject(err)
      });

      req.write( xml );
      req.end();
    })
  },

  /**
   * Function for getting transaction notification code, find transaction and update it
   * @param notification_code
   */

  callbackNotificationTransaction(notification_code){

      return this.getTransactionByNotificationCode(notification_code).then(pagseguroTransaction => {
        transactionService.getTransactionByCode(pagseguroTransaction.transaction.code).then(transaction => {

          transactionService.updateTransactionById(transaction._id,{status: pagseguroTransaction.transaction.status})
        }).catch(err=>{console.log(err)})
      }).catch(err=>{console.log(err)})

  },

  getTransactionByNotificationCode(notification_code){
    return new Promise((fulfill,reject)=>{
      const PAGSEGURO_NOTIFICATION_URL =

        `https://${HOST}/v3/transactions/notifications/${notification_code}`+
        `?email="${EMAIL}`+
        `&token="${TOKEN}`;

      request.get(PAGSEGURO_NOTIFICATION_URL,function(err, resp, body) {
        if(err)
          reject(err)

        const json = JSON.parse(xml2json.toJson(body));

        fulfill(json)
      })
    })
  }

}

module.exports = PagSeguroService
