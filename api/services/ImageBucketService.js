const fs = require("fs"),
      Storage = require('@google-cloud/storage'),
      config = require('./../../config/imageBucket.json')

const CLOUD_BUCKET = config.CLOUD_BUCKET;
const CLOUD_BUCKET_HOST = config.HOST;

const storage = Storage({
  projectId: config.GCLOUD_PROJECT,
  keyFilename: config.KEY_FILE_NAME
});

const bucket = storage.bucket(CLOUD_BUCKET);

const ImageBucketService = {

  sendUploadToGCS(file){
    const gcsname = Date.now() + file.name;
    const bucketFile = bucket.file(gcsname);
    const scope = this;

    return new Promise((fulfill,reject)=>{

      fs.createReadStream(file.path)
        .pipe(bucketFile.createWriteStream({
          metadata: {
            contentType: file.mimetype
          }
        }))
        .on("error", (err) => {
          reject(err)
        })
        .on('finish', () => {

          file.cloudStorageObject = gcsname;
          bucketFile.makePublic().then(() => {
            file.cloudStoragePublicUrl = scope.getPublicUrl(gcsname);
          })

          fulfill(this.getPublicUrl(gcsname))

        });

    })
  },

  getPublicUrl(filename){
    return `https://${CLOUD_BUCKET_HOST}/${CLOUD_BUCKET}/${filename}`;

  },

}

module.exports = ImageBucketService

