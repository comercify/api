const mongoose = require('mongoose'),
  service     = require('./Service'),
  Checkout     = mongoose.model('Checkout'),
  userService  = require('./UserService'),
  userProductService  = require('./UserProductService'),
  productService =  require('./ProductService'),
  pagSeguroService = require('./PagSeguroService'),
  transactionService = require('./TransactionService')

let checkoutService = require('./CheckoutService')

const CheckoutService = {

  createCheckout(user_id,user_products){

    return new Promise((fulfill,reject)=>{

      userProductService.createProducts(user_products).then(userProducts=>{

        const userProductsId = userProducts.map(product=>{
          return product._id
        })

        const checkoutModel = new Checkout({
          _user : user_id,
          _user_products: userProductsId
        })
        userService.addNewCheckout(checkoutModel._user,checkoutModel._id)

        service.create(checkoutModel).then(checkout=>{
          fulfill(checkout)

          }).catch(err=>{
            reject(err)
          })

      }).catch(err=>{
        reject(err)
      })

    })

  },
  updateCheckoutById(id,checkout){
    return service.updateById(Checkout,id,checkout)
  },
  getCheckoutById(id){
    return service.getByIdPopulating(Checkout,id, '_user_products','_product')
  },
  getCheckoutByTransactionCode(transaction_code){
    return new Promise((fulfill,reject)=>{
      transactionService.getTransactionByCode(transaction_code).then(transaction=>{
        Checkout.find({_transaction : transaction._id}), function(err, doc) {
          if (err)
            reject(err)
          fulfill(doc)
        }
      })
    })
  },
  getCheckoutSessionId(){
    return pagSeguroService.startSession()
  },
  addNewCheckoutTransaction(user_id,checkout_id){
    checkoutService = require('./CheckoutService')
    return new Promise((fulfill,reject)=> {
      this.getUserById(user_id).then(user=>{
        checkoutService.getCheckoutById(checkout_id).then(checkout=>{
          user._checkouts.push(checkout)
          fulfill(user.save())
        }).catch(err=>{
          reject(err)
        })
      }).catch(err=>{
        reject(err)
      })
    })

  },
  addTransaction(checkout_id, transaction_id){
    return new Promise((fulfill,reject)=> {
      this.getCheckoutById(checkout_id).then(checkout=>{
        checkout._transaction = transaction_id
        fulfill(checkout.save());
      }).catch(err=>{
        reject(err)
      })
    })

  },
  callbackNotification(notificationCode){
    return pagSeguroService.callbackNotificationTransaction(notificationCode)
  },

  processCheckoutRequest(checkout_id, user, address, shipping, payment, products){
    // const userPromisse = userService.updateUserById(user.id,user);
    //
    // const addressPromisse = addressService.createAddress(address);

    return new Promise((fulfill, reject)=>{
      productService.checkProducts(products).then(productsChecked=>{
        pagSeguroService.mountPaymentRequest(user, address, shipping, payment, productsChecked).then(xml=>{
          pagSeguroService.pay(xml).then(response=>{
            if(response.errors)
              return reject(response)

            const transaction = response.transaction
            const payment = {
              date: transaction.date,
              code: transaction.code,
              type: transaction.type,
              status: transaction.status,
              last_event_date: transaction.lastEventDate,
              payment_method_type: transaction.paymentMethod.type,
              payment_method_code: transaction.paymentMethod.code,
              gross_amount: transaction.grossAmount,
              discount_amount: transaction.discountAmount,
              fee_amount: transaction.feeAmount,
              net_amount: transaction.netAmount,
              extra_amount: transaction.extraAmount,
              installment_count: transaction.installmentCount,
              item_count: transaction.itemCount,
              _checkout: checkout_id
            }

            transactionService.createTransaction(payment)


            const data = {
              transaction_code: transaction.code,
              paymentLink:      transaction.paymentLink
            }
            fulfill(data)

          }).catch(err=>{
            reject(err)
          })
        }).catch(err=>{
          reject(err)
        })
      }).catch(err=>{
        reject(err)
      });
    })
  },



}

module.exports = CheckoutService

