

const DateHelper = {
  calcDeliveryDate: function(today,days){
    let count = 0;
    while (count < days) {
      today.setDate(today.getDate() + 1);
      if (today.getDay() !== 0 && today.getDay() !== 6) // Skip weekends
        count++;
    }
    return today;
  }




}

module.exports = DateHelper
