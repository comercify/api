'use strict'

const mongoose         = require('mongoose'),
  mongooseApiQuery = require('mongoose-api-query'),
  createdModified  = require('mongoose-createdmodified').createdModifiedPlugin

const SchemaTypes = mongoose.Schema.Types

const ProductSchema = new mongoose.Schema({
  name: {
    type 	 : String,
    required : true,
  },
  product:{
    type 	 : String,
    required : true,
  },
  color:{
    type 	 : String,
    required : false,
  },
  size:{
    type 	 : String,
    required : false,
  },
  description:{
    type 	 : String,
    required : false,
  },
  quantity:{
    type 	 : Number,
    required : true,
  },
  weight:{
    type 	 : Number,
    required : true,
  },
  height:{
    type 	 : Number,
    required : true,
  },
  width:{
    type 	 : Number,
    required : true,
  },
  length:{
    type 	 : Number,
    required : true,
  },
  tags: [{
    type: String,
    required: false,
  }],
  featured: {
    type: Boolean,
    required: false,
  },
  paid_value: {
    type 	 : Number,
    required : true,
  },
  sell_value: {
    type: String,
    required: true
  },
  image_url: {
    type 	 : String,
    required : false,
  },
  _category: {
      type 	 : mongoose.Schema.Types.ObjectId,
      ref 	 : 'Category',
      required : false
  },

});

ProductSchema.plugin(mongooseApiQuery)
ProductSchema.plugin(createdModified, { index: true })

const Product = mongoose.model('Product', ProductSchema)
module.exports = Product
