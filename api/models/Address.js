
'use strict'

const mongoose         = require('mongoose'),
  mongooseApiQuery = require('mongoose-api-query'),
  createdModified  = require('mongoose-createdmodified').createdModifiedPlugin

const AddressSchema = new mongoose.Schema({
  cep: {
    type 	 : String,
    required : true
  },
  logradouro: {
    type 	 : String,
    required : true
  },
  numero: {
    type 	 : Number,
    required : true
  },
  complemento:{
    type: String,
    required: false
  },
  bairro:{
    type: String,
    required: false
  },
  localidade:{
    type: String,
    required: true
  },
  uf: {
    type: String,
    required: true
  },
  type: {
    type: Number,
    required: false
  },
  cost: {
    type: Number,
    required: false
  },
  _checkout:{
    type 	 : mongoose.Schema.Types.ObjectId,
    ref 	 : 'Checkout',
    required : false
  }

});

AddressSchema.plugin(mongooseApiQuery)
AddressSchema.plugin(createdModified, { index: true })

const User = mongoose.model('Address', AddressSchema)
module.exports = User
