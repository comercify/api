
'use strict'

const mongoose         = require('mongoose'),
  mongooseApiQuery = require('mongoose-api-query'),
  createdModified  = require('mongoose-createdmodified').createdModifiedPlugin

const UserSchema = new mongoose.Schema({
  name: {
    type 	 : String,
    required : true,
  },
  last_name: {
    type 	 : String,
    required : true,
  },
  email: {
    type 	 : String,
    required : true,
  },
  cpf: {
    type 	 : String,
    required : false,
  },
  phone_number: {
    type: Number,
    required: false
  },
  area_code: {
    type: Number,
    required: false
  },
  password: {
    type: String,
    required: true
  },
  birth: {
    type 	 : String,
    required : false,
  },
  gender: {
    type: String,
    required: false
  },
  user_level: {
    type 	 : Number,
    required : true,
  },
  facebook: {
    type 	 : String,
    required : false,
  },

  cep: {
    type 	 : String,
    required : false,
  },
  logradouro: {
    type 	 : String,
    required : false,
  },
  complemento: {
    type 	 : String,
    required : false,
  },
  numero: {
    type 	 : String,
    required : false,
  },
  bairro: {
    type 	 : String,
    required : false,
  },
  localidade: {
    type 	 : String,
    required : false,
  },
  uf: {
    type 	 : String,
    required : false,
  },
  _checkouts: [{
    type 	 : mongoose.Schema.Types.ObjectId,
    ref 	 : 'Checkout',
    required : false
  }]


});

UserSchema.plugin(mongooseApiQuery)
UserSchema.plugin(createdModified, { index: true })

const User = mongoose.model('User', UserSchema)
module.exports = User
