'use strict'

const mongoose         = require('mongoose'),
  mongooseApiQuery = require('mongoose-api-query'),
  createdModified  = require('mongoose-createdmodified').createdModifiedPlugin

const SchemaTypes = mongoose.Schema.Types

const SaleSchema = new mongoose.Schema({
  _product: {
    type 	 : SchemaTypes.ObjectId,
    ref 	 : 'Products',
    required : false
  },
  start_at:{
    type: Date,
    required:false
  },
  end_at:{
    type: Date,
    required:false
  },
  sales_number:{
    type:Number,
    required: false,
  },
  value: {
    type: Number,
    required:false
  }

});

SaleSchema.plugin(mongooseApiQuery)
SaleSchema.plugin(createdModified, { index: true })

const Product = mongoose.model('Sale', SaleSchema)
module.exports = Product
