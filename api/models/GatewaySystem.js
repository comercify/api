'use strict'

const mongoose         = require('mongoose'),
  mongooseApiQuery = require('mongoose-api-query'),
  createdModified  = require('mongoose-createdmodified').createdModifiedPlugin

const SchemaTypes = mongoose.Schema.Types

const ProductSchema = new mongoose.Schema({
  type: {
    type 	 : String,
    required : true,
  },
  authorization_code:{
    type 	 : Number,
    required : true,
  },
  nsu:{
    type 	 : Number,
    required : false,
  },
  tid: {
    type 	 : Number,
    required : true,
  },
  establishment_code: {
    type: String,
    required: true
  },
  acquirer_name:{
    type: Number,
    required: true
  },

});

ProductSchema.plugin(mongooseApiQuery)
ProductSchema.plugin(createdModified, { index: true })

const Product = mongoose.model('Gateway-System', ProductSchema)
module.exports = Product
