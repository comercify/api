
'use strict'

const mongoose         = require('mongoose'),
  mongooseApiQuery = require('mongoose-api-query'),
  createdModified  = require('mongoose-createdmodified').createdModifiedPlugin

const TransactionSchema = new mongoose.Schema({
  date: {
    type 	 : Date,
  },
  code: {
    type 	 : String,
  },
  type: {
    type 	 : Number,
  },
  status: {
    type 	 : Number,
  },
  last_event_date: {
    type: Date,
  },
  payment_method_type: {
    type: Number,
  },
  payment_method_code: {
    type: String,
  },
  gross_amount: {
    type 	 : String,
    required : false,
  },
  discount_amount: {
    type: String,
    required: false
  },
  fee_amount: {
    type 	 : Number,
    required : true,
  },
  net_amount: {
    type 	 : String,
    required : false,
  },

  extra_amount: {
    type 	 : String,
    required : false,
  },
  installment_count: {
    type 	 : String,
    required : false,
  },
  item_count: {
    type 	 : String,
    required : false,
  },
  _checkout: {
    type 	 : mongoose.Schema.Types.ObjectId,
    ref 	 : 'Checkout',
    required : false
  }

  // gateway_system: {
  //   type 	 : SchemaTypes.ObjectId,
  //   ref 	 : 'Gateway-System',
  //   required : false
  // },

});

TransactionSchema.plugin(mongooseApiQuery)
TransactionSchema.plugin(createdModified, { index: true })

const Transaction = mongoose.model('Transaction', TransactionSchema)
module.exports = Transaction
