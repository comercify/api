'use strict'

const mongoose         = require('mongoose'),
  mongooseApiQuery = require('mongoose-api-query'),
  createdModified  = require('mongoose-createdmodified').createdModifiedPlugin

const SchemaTypes = mongoose.Schema.Types

const ShippingCartSchema = new mongoose.Schema({
  _product: {
    type 	 : SchemaTypes.ObjectId,
    ref 	 : 'Product',
    required : true
  },
  quantity:{
    type 	 : Number,
    required : true
  },
  value_paid:{
    type 	 : Number,
    required : true
  },

});

ShippingCartSchema.plugin(mongooseApiQuery)
ShippingCartSchema.plugin(createdModified, { index: true })

const Product = mongoose.model('User-Product', ShippingCartSchema)
module.exports = Product
