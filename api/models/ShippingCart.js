'use strict'

const mongoose         = require('mongoose'),
  mongooseApiQuery = require('mongoose-api-query'),
  createdModified  = require('mongoose-createdmodified').createdModifiedPlugin

const SchemaTypes = mongoose.Schema.Types

const ShippingCartSchema = new mongoose.Schema({
  _products: [{
    type 	 : SchemaTypes.ObjectId,
    ref 	 : 'Products',
    required : false
  }],
  _user: {
    type 	 : SchemaTypes.ObjectId,
    ref 	 : 'User',
    required : false
  }

});

ShippingCartSchema.plugin(mongooseApiQuery)
ShippingCartSchema.plugin(createdModified, { index: true })

const Product = mongoose.model('Shipping-Cart', ShippingCartSchema)
module.exports = Product
