
'use strict'

const mongoose         = require('mongoose'),
      mongooseApiQuery = require('mongoose-api-query'),
      createdModified  = require('mongoose-createdmodified').createdModifiedPlugin

const CategorySchema = new mongoose.Schema({
    name: {
        type 	 : String,
        required : true
    },
    _products: [{
      type 	 : mongoose.Schema.Types.ObjectId,
      ref 	 : 'Category',
      required : false
    }]
});

CategorySchema.plugin(mongooseApiQuery)
CategorySchema.plugin(createdModified, { index: true })

const User = mongoose.model('Category', CategorySchema)
module.exports = User
