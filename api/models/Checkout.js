
'use strict'

const mongoose         = require('mongoose'),
  mongooseApiQuery = require('mongoose-api-query'),
  createdModified  = require('mongoose-createdmodified').createdModifiedPlugin

const CheckoutSchema = new mongoose.Schema({
  _user_products: [{
    type 	 : mongoose.Schema.Types.ObjectId,
    ref 	 : 'User-Product',
    required : false
  }],
  _user: {
    type 	 : mongoose.Schema.Types.ObjectId,
    ref 	 : 'User',
    required : false
  },
  _shipping_address: {
    type 	 : mongoose.Schema.Types.ObjectId,
    ref 	 : 'Address',
    required : false
  },
  _transaction: {
    type 	 : mongoose.Schema.Types.ObjectId,
    ref 	 : 'Transaction',
    required : false
  },

});

CheckoutSchema.plugin(mongooseApiQuery)
CheckoutSchema.plugin(createdModified, { index: true })

const User = mongoose.model('Checkout', CheckoutSchema)
module.exports = User
