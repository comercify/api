
'use strict';


const controller     = require('./Controller'),
  _ 	               = require('lodash'),
  mongoose           = require('mongoose'),
  productService     = require('./../services/ProductService'),
  imageBucketService = require('./../services/ImageBucketService'),
  Product 	         = mongoose.model('Product')

const productController = {

  createProduct : function(req, res, next) {

    if(req.files){
      const data = req.body.body
      let jsonData = JSON.parse(data)
      const {image} = req.files;
      imageBucketService.sendUploadToGCS(image).then(url=>{

        jsonData.image_url = url;
        const product = new Product(jsonData)

        productService.createProduct(product).then(response=>{
          controller.returnResponseSuccess(res,response)
        })
      })
    }
    else {
      const data = req.body
      const product = new Product(data)

      productService.createProduct(product).then(response => {
        controller.returnResponseSuccess(res, response)
      })
    }

  },

  getAllProducts : function(req, res, next) {

    Product.find({}).exec(function(err,products){

      if(err)
        controller.returnResponseError(err,next)
      if(!products)
        controller.returnResponseNotFound(err,next)

      let productMap = {}

      products.forEach(function(product){
        productMap[product._id] = product
      })

      controller.returnResponseSuccess(res,productMap)
    })
  },

  getProductById : function(req, res, next) {
    const id = req.params.product_id
    controller.getById(Product, id, req, res, next)
  },

  searchProductByName : function(req, res, next){

    const name = req.params.name

    Product.find({name : {$regex : name, $options: "i" } }).exec(function(err,products){
      if(err)
        controller.returnResponseError(err,next)

      controller.returnResponseSuccess(res,products)
    })

  },

  updateProductById : function(req, res, next) {

    let data = req.body || {}
    let id   = req.params.product_id

    controller.updateById(Product, id, data, req, res, next)

  },

  deleteProductById : function(req, res, next) {

    let id = req.params.product_id
    controller.deleteById(Product, id, req, res, next)

  },
}

module.exports = productController
