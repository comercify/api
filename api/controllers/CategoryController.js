
'use strict';


const controller = require('./Controller'),
	_ 	     = require('lodash'),
	mongoose   = require('mongoose'),
	Category 	     = mongoose.model('Category')


const categoryController = {

	createCategory : function(req, res, next) {
		let data = req.body || {}

		let category = new Category(data)

		controller.create(category, req, res, next)
	},

	getAllCategories : function(req, res, next) {

		Category.find({}).exec(function(err,categorys){

			if(err)
				controller.returnResponseError(err,next)
			if(!categorys)
				controller.returnResponseNotFound(err,next)

			let categoryMap = {}

			categorys.forEach(function(category){
				categoryMap[category._id] = category
			})

			controller.returnResponseSuccess(res,categoryMap)
		})
	},

	getCategoryById : function(req, res, next) {

		const id = req.params.category_id
		controller.getById(Category, id, req, res, next)

	},

	searchCategoryByName : function(req, res, next){

		const name = req.params.name

		Category.find({name : {$regex : name, $options: "i" } }).exec(function(err,categorys){
			if(err)
				controller.returnResponseError(err,next)

			controller.returnResponseSuccess(res,categorys)
		})

	},

	updateCategoryById : function(req, res, next) {

		let data = req.body || {}
		let id   = req.params.category_id

		controller.updateById(Category, id, data, req, res, next)

	},

	deleteCategoryById : function(req, res, next) {

		let id = req.params.category_id
		controller.deleteById(Category, id, req, res, next)

	},
}

module.exports = categoryController
