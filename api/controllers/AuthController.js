'use strict';

const controller = require('./Controller'),
  mongoose       = require('mongoose'),
  User 	         = mongoose.model('User'),
  jwt            = require('jsonwebtoken')

const authController = {

  authenticate(req, res) {
    User.findOne({
      email: req.body.email
    }, function(err, user) {

      if (err) throw err;

      if (!user)
        return controller.returnResponseNotFound(res,'User not Found')

      if (user.password !== req.body.password)
        return controller.returnResponseNotFound(res, 'Wrong Password')

      const token = 'Bearer ' + jwt.sign(user, 'shhhhhhared-secret', {
        expiresIn : 60*60*24 // expires in 24 hours
      });

      const data = {
        token: token,
        user: user
      }

      return controller.returnResponseSuccess(res,data)

    });
  }

}


module.exports = authController
