'use strict';

const controller     = require('./Controller'),
  mongoose           = require('mongoose'),
  imageBucketService = require('./../services/ImageBucketService')


const imageController = {

  uploadImage(req, res, next) {
    const {image} = req.files;
    imageBucketService.sendUploadToGCS(image).then(url=>{
      return controller.returnResponseSuccess(res,url)
    })
  }

}


module.exports = imageController
