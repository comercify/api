'use strict';


const _ 	   = require('lodash'),
	  mongoose = require('mongoose')


const Controller = {

	returnResponseSuccess(res,data,msg){
		return res.json({
			success : true,
			data    : data,
			msg     : msg
		})

	},

	returnResponseError(res,err){
	  console.log(err)
    return res.json(500,{
      success : false,
      message : err.message,
      error   : err,
    })

	},

	returnResponseNotFound(res, msg){
    return res.json(401,{
      success : false,
      message : msg,
    })
	},

	create : function(type, req, res, next) {
		const scope = this
		type.save(function(err,doc) {
			if (err)
        scope.returnResponseError(res,err)

      scope.returnResponseSuccess(res,doc,'Created with Success')
		})
	},

	getAll : function(type, req, res, next) {
		type.apiQuery(req.params, function(err, docs) {
			if (err)
				this.returnResponseError(res,err)

			this.returnResponseSuccess(res,docs)

		})
	},

	getById : function(type, id, req, res, next) {

		const scope = this

		type.findOne({ _id: id }, function(err, doc) {

			if (err)
	        	scope.returnResponseError(err,next)

	      	scope.returnResponseSuccess(res,doc)

		})

	},

	updateById : function(type, id, data, req, res, next) {

		const scope = this
		type.findOneAndUpdate({ _id: id }, data, function(err, doc) {
			if (err)
				scope.returnResponseError(res,err)
			else if (!doc)
				scope.returnResponseNotFound(res,"Not Found")

			scope.returnResponseSuccess(res,data)

		})

	},

	deleteById : function(type, id, req, res, next) {

		const scope = this
		type.remove({ _id: id }, function(err) {

			if (err)
				scope.returnResponseError(err,next)

			scope.returnResponseSuccess(res,[],'deleted with success')


		})

	},

}

module.exports = Controller
