'use strict';

const DateHelper =require('../utils/DateHelper')


const controller     = require('./Controller'),
  Correios = require('node-correios'),
  correios = new Correios();

/**
 * nVlPeso: peso em kg
 * nCdFormato: formato da encomenda. 1 para pacote
 * @type {{nCdServico: [*], sCepOrigem: string, nVlPeso: number, nCdFormato: number, nVlComprimento: number, nVlAltura: number, nVlLargura: number, nVlDiametro: number, sCdMaoPropria: string, nVlValorDeclarado: string, sCdAvisoRecebimento: string}}
 */
const REQUEST_TYPE_SEDEX = {
  "nCdServico":  '40010,41106',
  "sCepOrigem":  "72220406",
  // "sCepDestino": null,
  "nVlPeso": .5,
  "nCdFormato":1,
  // "nVlComprimento":30,
  // "nVlAltura":20,
  // "nVlLargura":20,
  // "nVlDiametro":5,
  "sCdMaoPropria":"N",
  "nVlValorDeclarado":"0",
  "sCdAvisoRecebimento":"N"
}
const GMT = -3;

const MATCH_CODE = {
  41106 : 1,
  40010: 2

}
const correioController = {

  calcPrecoPrazo(req, res, next) {

    REQUEST_TYPE_SEDEX['sCepDestino'] = req.body.cep

    correios.calcPrecoPrazo(REQUEST_TYPE_SEDEX,(err,response) =>{
      if(err)
        return controller.returnResponseError(res,err)

      const resp = response.map(info=>{

        let today = new Date();
        today.setHours(today.getHours()+GMT);
        const days = info.PrazoEntrega
        return {
          code: MATCH_CODE[info.Codigo],
          deliveryDate: DateHelper.calcDeliveryDate(today,days),
          value: parseFloat(info.Valor.replace(',','.'))
        }
      })
      return controller.returnResponseSuccess(res,resp)
    })


  },

  calcPreco(req, res, next) {

    const json = req.params

    correios.calcPreco(json,(err,response) =>{
      if(err)
        return controller.returnResponseError(res,err)
      return controller.returnResponseSuccess(res,response)
    })
  },



}


module.exports = correioController
