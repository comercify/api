
'use strict';


const controller = require('./Controller'),
	_ 	       = require('lodash'),
	mongoose   = require('mongoose'),
	User 	     = mongoose.model('User'),
  userService = require('../services/UserService')

const userController = {

	createUser : function(req, res, next) {
		let data = req.body || {}

		let user = new User(data)

		controller.create(user, req, res, next)
	},

	getAllUsers : function(req, res, next) {
		User.find({}).exec(function(err,users){

			if(err)
				controller.returnResponseError(err,next)
			if(!users)
				controller.returnResponseNotFound(err,next)

			let userMap = {}

			users.forEach(function(user){
				userMap[user._id] = user
			})

			controller.returnResponseSuccess(res,userMap)
		})
	},

	getUserById : function(req, res, next) {

		const id = req.params.user_id
		controller.getById(User, id, req, res, next)

	},

	searchUserByName : function(req, res, next){

		const name = req.params.name

		User.find({name : {$regex : name, $options: "i" } }).exec(function(err,users){
			if(err)
				controller.returnResponseError(err,next)

			controller.returnResponseSuccess(res,users)
		})

	},

	updateUserById : function(req, res, next) {

		let data = req.body || {}
		let id   = req.params.user_id

    data.phone_number = data.phoneNumber || ''
    data.last_name = data.lastName || ''
    data.area_code = data.codeArea || ''
    delete data.password

    return userService.updateUserById(id,data).then(response=>{
      controller.returnResponseSuccess(res,response)
    })

	},

	deleteUserById : function(req, res, next) {

		let id = req.params.user_id
		controller.deleteById(User, id, req, res, next)

	},

  getCheckoutsByUserId: function(req, res, next) {

    let id = req.params.user_id
    userService.getCheckoutsByUserId(id).then(user=>{
      controller.returnResponseSuccess(res,user)
    }).catch(err=>{
      controller.returnResponseError(res,err)
    })

  }
}

module.exports = userController
