'use strict';

const controller = require('./Controller'),
  mongoose       = require('mongoose'),
  filterService  = require('../services/FilterService')

const filterController = {

  getFilterBoundaries(req, res, next) {
    filterService.getBoundaries().then(boundaries=>{
      controller.returnResponseSuccess(res,boundaries)
    })


  }

}


module.exports = filterController
