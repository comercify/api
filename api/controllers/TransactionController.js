
'use strict';


const controller = require('./Controller'),
	_ 	       = require('lodash'),
	mongoose   = require('mongoose'),
	Transaction 	     = mongoose.model('Transaction'),
  transactionService = require('../services/TransactionService')

const transactionController = {

	createTransaction : function(req, res, next) {
		let data = req.body || {}

		let transaction = new Transaction(data)

		controller.create(transaction, req, res, next)
	},

	getAllTransactions : function(req, res, next) {
		Transaction.find({}).exec(function(err,transactions){

			if(err)
				controller.returnResponseError(err,next)
			if(!transactions)
				controller.returnResponseNotFound(err,next)

			let transactionMap = {}

			transactions.forEach(function(transaction){
			  // transaction.remove({_id:transaction._id})
				transactionMap[transaction._id] = transaction
			})

			controller.returnResponseSuccess(res,transactionMap)
		})
	},

	getTransactionById : function(req, res, next) {

		const id = req.params.transaction_id
		controller.getById(Transaction, id, req, res, next)

	},

	searchTransactionByName : function(req, res, next){

		const name = req.params.name

		Transaction.find({name : {$regex : name, $options: "i" } }).exec(function(err,transactions){
			if(err)
				controller.returnResponseError(err,next)

			controller.returnResponseSuccess(res,transactions)
		})

	},

	updateTransactionById : function(req, res, next) {

		let data = req.body || {}
		let id   = req.params.transaction_id

    data.phone_number = data.phoneNumber || ''
    data.last_name = data.lastName || ''
    data.area_code = data.codeArea || ''
    delete data.password

    return transactionService.updateTransactionById(id,data).then(response=>{
      controller.returnResponseSuccess(res,response)
    })

	},

	deleteTransactionById : function(req, res, next) {

		let id = req.params.transaction_id
		controller.deleteById(Transaction, id, req, res, next)

	},

  getCheckoutsByTransactionId: function(req, res, next) {

    let id = req.params.transaction_id
    transactionService.getCheckoutsByTransactionId(id).then(transaction=>{
      controller.returnResponseSuccess(res,transaction)
    }).catch(err=>{
      controller.returnResponseError(res,err)
    })

  }
}

module.exports = transactionController
