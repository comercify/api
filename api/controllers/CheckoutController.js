'use strict';

const controller     = require('./Controller'),
  mongoose           = require('mongoose'),
  Checkout 	     = mongoose.model('Checkout'),
  checkoutService = require('../services/CheckoutService')

const CHECKOUT_METHODS = {
  1: "creditCard",
  2: 'boleto'
}
const checkoutController = {

  checkout(req, res, next) {
    const data = req.body || {}

    const installment = {
      quantity: data.payment.installment ? data.payment.installment.quantity : '',
      noInterestInstallmentQuantity: data.payment.installment ? data.payment.installment.noInterestInstallmentQuantity : '',
      value: data.payment.installment ? data.payment.installment.installmentAmount : ''
    }

    const user = {
        _id: data.user.id,
        name: data.user.name,
        last_name: data.user.lastName,
        email: data.user.email,
        cpf: data.user.cpf,
        birth: data.user.birth,
        gender: data.user.gender,
        sender_hash: data.user.hash,
        phone_number: data.user.phoneNumber,
        area_code: data.user.codeArea
      },
      address = {
        cep : data.address.cep,
        logradouro : data.address.logradouro,
        complemento : data.address.complemento,
        numero: data.address.numero,
        bairro: data.address.bairro,
        localidade: data.address.localidade,
        uf: data.address.uf
      },
      payment = {
        method: CHECKOUT_METHODS[data.method],
        token: data.payment.token,
        value: data.payment.value,
        installment: {
          quantity: installment.quantity,
          noInterestInstallmentQuantity: installment.noInterestInstallmentQuantity,
          value: installment.quantity.value
        },
      },
      shipping ={
        cost: data.shipping.cost,
        type: data.shipping.type,
        delivery_date: data.shipping.deliveryDate
      },
      products = data.products,
      checkout_id = data.checkoutId;

    let productsArr = [];

    for(let i in products){
      productsArr.push(products[i])
    }

    productsArr = productsArr.map(product=>{
      return {
        _id: product.id,
        sell_value: product.price,
        qtd: product.qtd
      }
    })

    checkoutService.processCheckoutRequest(checkout_id,user,address,shipping,payment,productsArr).then(response=>{
      controller.returnResponseSuccess(res,response)
    }).catch(err=>{
      controller.returnResponseError(res,err)
    })
  },
  startCheckoutSession : function(req, res, next) {
    checkoutService.getCheckoutSessionId().then(response =>{
      controller.returnResponseSuccess(res,response)
    })
  },

  createCheckout(req, res, next){
    const data = req.body || {}

    const products = data.products

    const userProducts = products.map(product => {
      return {
        _product   : product.id,
        quantity   : product.qtd,
        value_paid : product.value,
      }
    })

    checkoutService.createCheckout(data.userId, userProducts).then(createdCheckout=>{
      return controller.returnResponseSuccess(res,createdCheckout)
    })

  },

  getAllCheckouts : function(req, res, next) {
    Checkout.find({}).populate('_user _products _transaction').exec(function(err,checkouts){

      if(err)
        controller.returnResponseError(err,next)
      if(!checkouts)
        controller.returnResponseNotFound(err,next)

      let checkoutMap = {}

      checkouts.forEach(function(checkout){
        checkoutMap[checkout._id] = checkout
      })

      return controller.returnResponseSuccess(res,checkoutMap)
    })
  },

  deleteAllCheckouts : function(req, res, next) {
    Checkout.find({}).populate('_user _products _transaction').exec(function(err,checkouts){

      if(err)
        controller.returnResponseError(err,next)
      if(!checkouts)
        controller.returnResponseNotFound(err,next)

      let checkoutMap = {}

      checkouts.forEach(function(checkout){
        checkout.remove({_id:checkout._id})
      })

      return controller.returnResponseSuccess(res,checkoutMap)
    })
  },

  getCheckoutById : function(req, res, next) {

    const id = req.params.checkout_id

    checkoutService.getCheckoutById(id).then(checkout=>{
      return controller.returnResponseSuccess(res,checkout)
    })

  },

  callbackTransaction(req, res, next){
    const data = req.body || {}
    checkoutService.callbackNotification(data.notificationCode).then(()=>{
      return controller.returnResponseSuccess(res,{},'transaction status updated with success')
    }).catch(err=>{
      console.log(err)
    })
  },

  searchCheckoutByName : function(req, res, next){

    const name = req.params.name

    Checkout.find({name : {$regex : name, $options: "i" } }).exec(function(err,checkouts){
      if(err)
        controller.returnResponseError(err,next)

      controller.returnResponseSuccess(res,checkouts)
    })

  },

  updateCheckoutById : function(req, res, next) {

    let data = req.body || {}
    let id   = req.params.checkout_id

    controller.updateById(Checkout, id, data, req, res, next)

  },

  deleteCheckoutById : function(req, res, next) {

    let id = req.params.checkout_id
    controller.deleteById(Checkout, id, req, res, next)

  },



}


module.exports = checkoutController
