/**
 * user_level 0 is admin level
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
const verifyAdmin = function(req, res, next){

  if(req.user._doc.user_level === 0)
    return next();

  //remove later
  return next()

  return res.json(401, {
    success: false,
    message: "You need to be an ADMIN to have access in here"
  })

}

module.exports = verifyAdmin
