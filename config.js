
'use strict'

/**AWS**/
// "DB_HOST":  "mongodb://ec2-13-59-2-20.us-east-2.compute.amazonaws.com:27017/commercify-puffpass",

/**HEROKU**/
//  "DB_HOST"    : "mongodb://vctt:PuffPass0@e-commerce-shard-00-00-vqcfi.mongodb.net:27017,e-commerce-shard-00-01-vqcfi.mongodb.net:27017,e-commerce-shard-00-02-vqcfi.mongodb.net:27017/e-commerce?ssl=true&replicaSet=e-commerce-shard-0&authSource=admin",

const devEnv     = require('./config/devEnv.json')
const mainConfig = require('./config/mainConfig.json')
const dbConfig   = require('./config/DbConfig.json')

if(!process.env.NODE_ENV)
    process.env.NODE_ENV = devEnv.NODE_ENV

module.exports = {
    name: mainConfig.NAME,
    version: mainConfig.VERSION,
    env: process.env.NODE_ENV ,
    port: process.env.PORT || devEnv.PORT,
    base_url: process.env.BASE_URL || devEnv.BASE_URL,
    db: {
        // uri: 'mongodb://127.0.0.1:27017/API_APP_BATALHA',
        uri: dbConfig.URI,
    },
}
